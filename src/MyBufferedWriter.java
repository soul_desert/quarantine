import java.io.BufferedWriter;
import java.io.IOException;
import java.io.Writer;

/**
 * Created by Alex on 07.12.2016.
 */
public class MyBufferedWriter extends BufferedWriter {

    private double numberOfLines;

    public MyBufferedWriter(Writer out) {
        super(out);
        numberOfLines = 0;
    }

    @Override
    public void newLine() throws IOException {
        super.newLine();
        numberOfLines++;
    }

    public double getNumberOfLines() {
        return numberOfLines;
    }
}
