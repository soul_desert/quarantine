import java.io.*;
import java.util.LinkedList;
import java.util.Queue;

/**
 * Created by Alex on 30.11.2016.
 */
public class Main {
    public static void main(String[] args) throws IOException {

        MyBufferedReader inputFile = new MyBufferedReader(new FileReader("data/inputFile.txt"));
        MyBufferedWriter outputFile = new MyBufferedWriter(new FileWriter("data/outputFile.txt", true));

        int queueSize = 8;

        Queue buf1 = new QuarantineQueue(queueSize);
        Queue buf2 = new QuarantineQueue(queueSize);

        while (true) {

            Thread one = new Thread(new FileInStream(inputFile, buf1));
            one.run();

            Thread two = new Thread(new QueueStream(buf1, buf2));
            two.run();

            Thread three = new Thread(new FileOutStream(outputFile, buf2));
            three.run();

        }

        //System.out.println();

    }
}
