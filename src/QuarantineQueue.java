import java.util.LinkedList;

/**
 * Created by Alex on 03.12.2016.
 */
public class QuarantineQueue extends LinkedList {

    private int maxSize;

    public QuarantineQueue(int maxSize) {
        super();
        this.maxSize = maxSize;
    }

    @Override
    public boolean add(Object o) {
        if(size() == maxSize) {
            removeLast();
        }
        return super.add(o);
    }
}
