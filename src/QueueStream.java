import java.util.Queue;

/**
 * Created by Alex on 30.11.2016.
 */
public class QueueStream implements Runnable {

    private static Queue buf1;
    private static Queue buf2;


    public QueueStream(Queue buf1, Queue buf2) {
        this.buf1 = buf1;
        this.buf2 = buf2;
    }

    @Override
    public void run() {
        String buffer;
        synchronized (buf1) {

            try {
                buffer = buf1.poll().toString();

                synchronized (buf2) {
                    buf2.add(buffer);
                }

            } catch (NullPointerException e) {
                // игнорировать
            }

        }

    }
}
