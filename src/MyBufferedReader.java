import java.io.BufferedReader;
import java.io.Reader;

/**
 * Created by Alex on 07.12.2016.
 */
public class MyBufferedReader extends BufferedReader {

    private static double numberOfLines;

    public MyBufferedReader(Reader in) {
        super(in);
        numberOfLines = 0;
    }

    public static double getNumberOfLines() {
        return numberOfLines;
    }

    public static void setNumberOfLines(double numberOfLines) {
        MyBufferedReader.numberOfLines = numberOfLines;
    }
}
