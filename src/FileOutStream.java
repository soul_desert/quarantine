import java.io.IOException;
import java.util.Queue;

/**
 * Created by Alex on 30.11.2016.
 */
public class FileOutStream implements Runnable {

    private static MyBufferedWriter outputFile;
    private static Queue buf2;

    public FileOutStream(MyBufferedWriter outputFile, Queue buf2) {
        this.outputFile = outputFile;
        this.buf2 = buf2;
    }


    @Override
    public void run() {
        synchronized (buf2) {

            if(buf2.isEmpty()) {
                try {
                    outputFile.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                double loss = 1 - outputFile.getNumberOfLines()/MyBufferedReader.getNumberOfLines();
                System.out.println("Потеряно: " + loss * 100 + "% информации");
                System.exit(1);
            } else {
                String buffer = buf2.poll().toString();
                try {
                    outputFile.append(buffer);
                    outputFile.newLine();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

        }
    }
}
