import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Queue;

/**
 * Created by Alex on 30.11.2016.
 */
public class FileInStream implements Runnable {

    private static MyBufferedReader inputFile;
    private static Queue buf1;

    public FileInStream(MyBufferedReader inputFile, Queue buf1) {
        this.inputFile = inputFile;
        this.buf1 = buf1;
    }

    @Override
    public void run() {
        synchronized (buf1) {
            try {
                String readed = inputFile.readLine();
                if(readed != null) {
                    addProcessed(readed);
                    readed = inputFile.readLine();
                    if(readed != null) addProcessed(readed);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            try {
                Thread.sleep(20);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    private void addProcessed(String readed) {
        List<String> digits = new ArrayList<>();
        for (int i = 0; i < 10; i++) digits.add(String.valueOf(i)) ;
        char[] symbols = readed.toCharArray();
        String processed = "";
        for (char symbol : symbols) {
            if (digits.contains(String.valueOf(symbol))) processed += String.valueOf(symbol);
        }
        buf1.add(processed);
        MyBufferedReader.setNumberOfLines(MyBufferedReader.getNumberOfLines() + 1);
    }

}

